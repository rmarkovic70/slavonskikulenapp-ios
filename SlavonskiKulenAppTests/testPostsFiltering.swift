//
//  testPostsFiltering.swift
//  SlavonskiKulenAppTests
//
//  Created by Apis IT on 07.03.2024..
//

import XCTest
@testable import SlavonskiKulenApp

final class testPostsFiltering: XCTestCase {
    
    func testSuccessfulPostsFilteringWithFilterInput() {
        // Arrange
        let postsVM = PostsViewModel()
        let post1 = Post(userId: 1, id: 1, title: "Backyard flowers", body: "Flower is red")
        let post2 = Post(userId: 2, id: 2, title: "They walked for miles across steep and inhospitable terrain", body: "Terrain can be dangerous")
        let post3 = Post(userId: 3, id: 3, title: "Rocky mountains national park", body: "Up to heights 1,800 to 4,400 m above sea level")
        
        postsVM.displayedPosts = [post1, post2, post3]
        postsVM.posts = [post1, post2, post3]
        
        // Act
        postsVM.filterPosts(with: "roc")
        
        // Assert
        XCTAssertTrue((postsVM.displayedPosts.first?.title != nil), "Rocky mountains national park")
    }
    
    func testSuccessfulPostsFilteringWithEmptyFilterInput() {
        // Arrange
        let postsVM = PostsViewModel()
        let post1 = Post(userId: 1, id: 1, title: "Backyard flowers", body: "Flower is red")
        let post2 = Post(userId: 2, id: 2, title: "They walked for miles across steep and inhospitable terrain", body: "Terrain can be dangerous")
        let post3 = Post(userId: 3, id: 3, title: "Rocky mountains national park", body: "Up to heights 1,800 to 4,400 m above sea level")
        
        postsVM.displayedPosts = [post1, post2, post3]
        postsVM.posts = [post1, post2, post3]
        
        // Act
        postsVM.filterPosts(with: "qwErtzUio")
        
        // Assert
        XCTAssertTrue(postsVM.displayedPosts.isEmpty, "Filtered posts array empty.")
    }

}
