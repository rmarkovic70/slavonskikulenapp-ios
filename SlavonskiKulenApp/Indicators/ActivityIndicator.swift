//
//  ActivityIndicator.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 02.02.2024..
//

import UIKit

class SlavonskiKulenActivityIndicator {
    
    private var activityIndicator = UIActivityIndicatorView()
    
    init() {
        activityIndicator.style = .large
        activityIndicator.color = Design.Colors.Primary.kulenWhite
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show(in parentView: UIView) {
        parentView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        parentView.isUserInteractionEnabled = false
        activityIndicator.centerXAnchor.constraint(equalTo: parentView.centerXAnchor).isActive = true
        activityIndicator.centerYAnchor.constraint(equalTo: parentView.centerYAnchor).isActive = true
    }
    
    func hide(from parentView: UIView?) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1)) {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.removeFromSuperview()
        }
        parentView?.isUserInteractionEnabled = true
    }
}
