//
//  RefreshControl.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 07.02.2024..
//

import UIKit

class SlavonskiKulenRefreshControl {
    
    private var refreshControl = UIRefreshControl()
    var onRefresh: (() -> ())?
    
    init() {
        refreshControl.tintColor = Design.Colors.Primary.kulenWhite
        refreshControl.translatesAutoresizingMaskIntoConstraints = false
        refreshControl.addTarget(self, action: #selector(onRefreshAction), for: UIControl.Event.valueChanged)
    }
    
    @objc private func onRefreshAction() {
        onRefresh?()
    }
    
    func add(to parentView: UITableView) {
        parentView.addSubview(refreshControl)
    }
    
    func start() {
        refreshControl.beginRefreshing()
    }
    
    func stop() {
        refreshControl.endRefreshing()
    }
}
