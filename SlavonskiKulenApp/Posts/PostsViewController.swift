//
//  ViewController.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 14.12.2023..
//

import UIKit
import Combine

class PostsViewController: UIViewController {
    
    var didSelectPost: ((Post) -> ())?
    let postsViewModel = PostsViewModel()
    let activityIndicator = SlavonskiKulenActivityIndicator()
    var refreshControl = SlavonskiKulenRefreshControl()
    var popUpAlert: (() -> ())?
    private var subscriptionBag = Set<AnyCancellable>()

    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.register(PostsTableViewCell.self, forCellReuseIdentifier: PostsTableViewCell.identifier)
        table.backgroundColor = Design.Colors.Primary.kulenLightGrey
        table.separatorColor = Design.Colors.Primary.kulenLightGrey
        table.translatesAutoresizingMaskIntoConstraints = false
        table.layer.cornerRadius = 10
        table.delegate = self
        table.dataSource = self
        return table
    }()
    
    private lazy var searchBar: UISearchBar = {
       let searchBar = UISearchBar()
        searchBar.searchTextField.attributedPlaceholder = NSAttributedString(string: "Search names", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        searchBar.searchTextField.clearButtonMode = .always
        searchBar.searchTextField.textColor = .white
        searchBar.barTintColor = Design.Colors.Primary.kulenDarkGray
        searchBar.searchTextField.backgroundColor = Design.Colors.Primary.kulenLightGrey
        searchBar.tintColor = .white
        searchBar.searchTextField.leftView?.tintColor = .white
        searchBar.searchTextField.layer.cornerRadius = 10
        searchBar.delegate = self
        searchBar.searchTextField.layer.masksToBounds = true
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchBar
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Design.Colors.Primary.kulenDarkGray
        refreshControl.add(to: self.tableView)
        setupRefreshControl()
        addSubviews()
        bind()
        postsViewModel.fetchPostData()
    }

    private func bind() {
        postsViewModel.state = .loading
        
        postsViewModel.$displayedPosts
            .combineLatest(postsViewModel.$state)
            .receive(on: RunLoop.main)
            .sink { [weak self] ( _ , _ ) in
                self?.reloadState()
            }
            .store(in: &subscriptionBag)
        
        postsViewModel.textFieldPublisher
            .receive(on: RunLoop.main)
            .sink { filterInput in
                self.postsViewModel.filterPosts(with: filterInput)
                self.postsViewModel.state = .loaded
            }
            .store(in: &subscriptionBag)
    }
    
    func reloadState() {
        switch postsViewModel.state {
        case .empty:
            ()
        case .loading:
            activityIndicator.show(in: self.tableView)
        case .loaded:
            reloadTableData()
            activityIndicator.hide(from: self.tableView)
        }
    }
    
    private func reloadTableData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func setupRefreshControl() {
        refreshControl.onRefresh = { [weak self] in
            DispatchQueue.main.async {
                self?.refreshControl.start()
                self?.postsViewModel.fetchPostData()
                self?.reloadTableData()
                self?.refreshControl.stop()
            }
        }
    }
    
    private func addSubviews() {
        view.addSubview(searchBar)
        view.addSubview(tableView)
        constraint()
    }
    
    private func constraint() {
        searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        searchBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        searchBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 8).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
    }
}

extension PostsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PostsTableViewCell.identifier, for: indexPath) as? PostsTableViewCell else {
            return UITableViewCell()
        }
        let data = postsViewModel.displayedPosts[indexPath.row]
        cell.setupUI(post: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { _, _, _ in
            tableView.beginUpdates()
            self.postsViewModel.posts.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
        delete.backgroundColor = Design.Colors.Secondary.kulenRed
        let swipe = UISwipeActionsConfiguration(actions: [delete])
        return swipe
    }
}

extension PostsViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postsViewModel.displayedPosts.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = postsViewModel.posts[indexPath.row]
        didSelectPost?(data)
    }
}

extension PostsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        postsViewModel.displayedPosts = postsViewModel.posts
        if searchBar.searchTextField.text == "" {
            popUpAlert?()
        } else {
            if let filterInput = searchBar.searchTextField.text { postsViewModel.textFieldPublisher.send(filterInput) }
        }
    }
}
