//
//  AboutPostsViewController.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 03.01.2024..
//

import UIKit

class AboutPostsViewController: UIViewController {
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .leading
        stack.distribution = .equalSpacing
        stack.layer.cornerRadius = 10
        stack.spacing = 10
        stack.backgroundColor = Design.Colors.Primary.kulenDarkGray
        stack.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        stack.isLayoutMarginsRelativeArrangement = true
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var userIdLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var postIdLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var postTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var postBodyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 0
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSelfView()
        addSubviews()
    }

    func setupSelfView() {
        view.backgroundColor = Design.Colors.Primary.kulenLightGrey
    }
   
    private func addSubviews() {
        stackView.addArrangedSubview(userIdLabel)
        stackView.addArrangedSubview(postIdLabel)
        stackView.addArrangedSubview(postTitleLabel)
        stackView.addArrangedSubview(postBodyLabel)
        view.addSubview(stackView)
        constraint()
    }
    
    private func constraint() {
        stackView.topAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        stackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        stackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        stackView.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
    }
    
    func setupUI(post: Post) {
        userIdLabel.text = "User ID: \(String(post.userId ?? -1))"
        postIdLabel.text = "Post ID: \(String(post.id ?? -1))"
        postTitleLabel.text = "Title: \(post.title ?? "Title error")"
        postBodyLabel.text = "Body: \(post.body ?? "Body error")"
    }
}
