//
//  HomeViewModel.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 14.12.2023..
//
import UIKit
import Combine

class PostsViewModel: ObservableObject {
    
    @Published var displayedPosts: [Post] = []
    var textFieldPublisher = PassthroughSubject<String, Never>()
    var posts: [Post] = []
    private var bag = Set<AnyCancellable>()
    private var filteredPostsBag = Set<AnyCancellable>()
    @Published var state: State = .empty
    
    func fetchPostData() {
        ApiCaller.shared.fetchPosts()
            .sink(
                receiveCompletion: { _ in
                }, receiveValue: { result in
                    self.displayedPosts = result
                    self.posts = result
                    self.state = .loaded
                })
            .store(in: &bag)
    }
    
    func filterPosts(with filterInput: String) {
        if filterInput.isEmpty || filterInput.starts(with: " ") {
            displayedPosts = posts
            return
        }
        displayedPosts = displayedPosts.filter({ post in
            let title = post.title?.lowercased()
            return title?.starts(with: filterInput.lowercased()) ?? false
        })
    }
}
