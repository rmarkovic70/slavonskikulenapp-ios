//
//  HomeTableViewCell.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 14.12.2023..
//

import UIKit

class PostsTableViewCell: UITableViewCell {

    static let identifier: String = "postCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        contentView.backgroundColor = Design.Colors.Primary.kulenLightGrey
        addSubviews()
    }
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .leading
        stack.distribution = .equalSpacing
        stack.layer.cornerRadius = 10
        stack.spacing = 10
        stack.backgroundColor = Design.Colors.Primary.kulenDarkGray
        stack.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        stack.isLayoutMarginsRelativeArrangement = true
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var userIdLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var postIdLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var postTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var postBodyLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 0
        return label
    }()
    
    func setupUI(post: Post) {
        userIdLabel.text = "UserID: \(String(post.userId ?? -1))"
        postIdLabel.text = "PostID: \(String(post.id ?? -1))"
        postTitleLabel.text = "Title: \(post.title ?? "Title error")"
        postBodyLabel.text = "Body: \(post.body ?? "Body error")"
    }
    
    private func addSubviews() {
        stackView.addArrangedSubview(userIdLabel)
        stackView.addArrangedSubview(postIdLabel)
        stackView.addArrangedSubview(postTitleLabel)
        stackView.addArrangedSubview(postBodyLabel)
        contentView.addSubview(stackView)
        constraint()
    }
    
    private func constraint() {
        stackView.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor, constant: 5).isActive = true
        stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        stackView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -5).isActive = true
    }
}
