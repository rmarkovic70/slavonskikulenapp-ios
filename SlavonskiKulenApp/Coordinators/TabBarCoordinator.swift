//
//  TabBarCoordinator.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 21.02.2024..
//

import UIKit

class TabBarCoordinator: BaseCoordinator {
    
    let keychainKeyStorage: UserKeyStorage
    let keychainLoginStatusStorage: LoginStatusStorage
    var postsCoordinator: PostsCoordinator
    var receiverCoordinator: ReceiverCoordinator
    var usersCoordinator: UsersCoordinator
    var userProfileCoordinator: UserProfileCoordinator
    var logout: (()->())?
    
    init(keychainKeyStorage: UserKeyStorage, keychainLoginStatusStorage: LoginStatusStorage) {
        self.keychainKeyStorage = keychainKeyStorage
        self.keychainLoginStatusStorage = keychainLoginStatusStorage
        
        self.postsCoordinator = PostsCoordinator()
        self.receiverCoordinator = ReceiverCoordinator()
        self.usersCoordinator = UsersCoordinator()
        self.userProfileCoordinator = UserProfileCoordinator(keychainKeyStorage: keychainKeyStorage, keychainLoginStatusStorage: keychainLoginStatusStorage)
    }
    
    func start() -> UIViewController {
        let slavonskiKulenTabBarController = UITabBarController()
        slavonskiKulenTabBarController.tabBar.barTintColor = Design.Colors.Primary.kulenDarkGray
        slavonskiKulenTabBarController.tabBar.tintColor = Design.Text.Colors.kulenWhite
        
        postsCoordinator.start()
        receiverCoordinator.start()
        usersCoordinator.start()
        userProfileCoordinator.start()
        userProfileCoordinator.toMainScreen = { [weak self] in
            self?.logout?()
        }
        
        slavonskiKulenTabBarController.viewControllers = [postsCoordinator.navigationController, usersCoordinator.navigationController,  receiverCoordinator.navigationController, userProfileCoordinator.navigationController]
        slavonskiKulenTabBarController.viewControllers?[0].tabBarItem.title = "Posts"
        slavonskiKulenTabBarController.viewControllers?[1].tabBarItem.title = "Users"
        slavonskiKulenTabBarController.viewControllers?[2].tabBarItem.title = "Send message"
        slavonskiKulenTabBarController.viewControllers?[3].tabBarItem.title = "My profile"
        
        return slavonskiKulenTabBarController
    }
}
