//
//  MainCoordinator.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 28.12.2023..
//

import UIKit

enum LoginStatus: String {
    case loggedIn = "loggedIn"
    case loggedOut = "loggedOut"
}

class MainCoordinator: BaseCoordinator {
    
    let window: UIWindow
    let keychainLoginStateStorage: LoginStatusStorage
    let loginCoordinator: LoginCoordinator
    
    init(window: UIWindow) {
        self.window = window
        self.keychainLoginStateStorage = KeychainLoginStatusStorage(service: "LOGIN_service", account: "main_account")
        self.loginCoordinator = LoginCoordinator(window: window, keychainLoginStateStorage: keychainLoginStateStorage)
    }
    
    override func start() {
        let loginState = keychainLoginStateStorage.loadLoginStatus()
        switch loginState {
        case "loggedOut":
            setLoginFlow()
        case "loggedIn":
            setAuthFlow()
        default:
            ()
        }
        
        loginCoordinator.loginViewModel.resetAndLogout = { [weak self] in
            self?.setLoginFlow()
        }
        loginCoordinator.tabBarCoordinator.logout = { [weak self] in
            self?.setLoginFlow()
        }
    }
    
    func setLoginFlow() {
        window.rootViewController = loginCoordinator.startLogin()
    }
    func setAuthFlow() {
        window.rootViewController = loginCoordinator.startAuth()
    }
}

/// Deprecated storages:
//let UDLoginStateStorage = UserDefaultsLoginStatusStorage(base: UserDefaults.standard, loginKey: "LoginKey")
//let UDKeyStorage = UserDefaultsKeyStorage(base: UserDefaults.standard, pinKey: "PIN")
