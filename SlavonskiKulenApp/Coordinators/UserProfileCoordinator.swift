//
//  UserProfileCoordinator.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 03.03.2024..
//

import UIKit

class UserProfileCoordinator: BaseCoordinator {
    
    let navigationController: UINavigationController
    let keychainKeyStorage: UserKeyStorage
    let keychainLoginStatusStorage: LoginStatusStorage
    var toMainScreen: () -> ()
    
    init(keychainKeyStorage: UserKeyStorage, keychainLoginStatusStorage: LoginStatusStorage) {
        self.navigationController = UINavigationController()
        self.keychainKeyStorage = keychainKeyStorage
        self.keychainLoginStatusStorage = keychainLoginStatusStorage
        self.toMainScreen = {}
    }
    
    override func start() {
        let userProfileVC = UserProfileViewController(keychainKeyStorage: keychainKeyStorage, keychainLoginStatusStorage: keychainLoginStatusStorage, toMainScreen: {})
        userProfileVC.toMainScreen = { [weak self] in
            self?.toMainScreen()
        }
        navigationController.setViewControllers([userProfileVC], animated: false)
    }
}
