//
//  LoginNavigator.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 19.02.2024..
//

import UIKit

class LoginCoordinator: BaseCoordinator {
    
    let window: UIWindow
    let keychainKeyStorage: KeychainKeyStorage
    let keychainLoginStateStorage: LoginStatusStorage
    let loginViewModel: LoginViewModel
    let tabBarCoordinator: TabBarCoordinator
    
    init(window: UIWindow, keychainLoginStateStorage: LoginStatusStorage) {
        self.window = window
        self.keychainKeyStorage = KeychainKeyStorage(service: "PIN_service", account: "main_account")
        self.keychainLoginStateStorage = keychainLoginStateStorage
        self.loginViewModel = LoginViewModel(pinToken: "", userPIN: "", digits: [], keyStorage: keychainKeyStorage, loginStatusStorage: keychainLoginStateStorage) {} resetAndLogout: {}
        self.tabBarCoordinator = TabBarCoordinator(keychainKeyStorage: keychainKeyStorage, keychainLoginStatusStorage: keychainLoginStateStorage)
    }
    
    func startLogin() -> UIViewController {
        let loginVC = LoginViewController()
        loginVC.didSelectLogin = { [weak self] in
            self?.window.rootViewController = self?.startAuth()
        }
        return loginVC
    }
    
    func startAuth() -> UIViewController {
        setupCTabBarCoordinator()
        let pinVC = PINViewController(loginModel: loginViewModel)
        return pinVC
    }
    
    func setupCTabBarCoordinator() {
        loginViewModel.loginSuccessful =  { [weak self] in
            self?.window.rootViewController = self?.tabBarCoordinator.start()
        }
    }
}
