//
//  ReceiverCoordinator.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 01.02.2024..
//

import UIKit

class ReceiverCoordinator: BaseCoordinator {
    let navigationController: UINavigationController
    
    override init() {
        self.navigationController = UINavigationController()
    }
    
    override func start() {
        let receiverVC = ReceiverViewController()
        
        receiverVC.didTapButton = { [weak self] model in
            let senderVC = SenderViewController()
            senderVC.textfieldViewModel = model
            self?.navigationController.pushViewController(senderVC, animated: true)
            
            senderVC.didTapButton = { [weak self] in
                self?.navigationController.popViewController(animated: true)
            }
        }
        navigationController.setViewControllers([receiverVC], animated: false)
    }
}
