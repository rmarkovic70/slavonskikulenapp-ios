//
//  UsersCoordinator.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 01.02.2024..
//

import UIKit

class UsersCoordinator: BaseCoordinator {
    let navigationController: UINavigationController
    var usersViewModel: UsersViewModel?
    
    override init() {
        self.navigationController = UINavigationController()
    }
    
    override func start() {
        let usersVC = UsersViewController()
        usersVC.popUpAlert = { [weak self] in
            let emptyFilterAlert = Alerts.manager.createBasicAlert(title: "Search empty", message: "Please provide search keywords")
            self?.navigationController.present(emptyFilterAlert, animated: true)
        }
        navigationController.setViewControllers([usersVC], animated: false)
    }
}
