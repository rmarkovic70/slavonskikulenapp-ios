//
//  RootCoordinator.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 28.12.2023..
//

import UIKit

class BaseCoordinator: NSObject {
    
    override init() {}
    
    func start() {}
}
