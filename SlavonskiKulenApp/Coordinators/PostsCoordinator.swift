//
//  PostsCoordinator.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 01.02.2024..
//

import UIKit

class PostsCoordinator: BaseCoordinator {
    let navigationController: UINavigationController
    
    override init() {
        self.navigationController = UINavigationController()
    }
    
    override func start() {
        let postsVC = PostsViewController()
        postsVC.didSelectPost = { [weak self] data in
            self?.pushDetailsScreen(data: data)
        }
        navigationController.setViewControllers([postsVC], animated: false)
    }
    
    private func pushDetailsScreen(data: Post) {
        let detailsVC = AboutPostsViewController()
        detailsVC.setupUI(post: data)
        self.navigationController.pushViewController(detailsVC, animated: true)
    }
}
