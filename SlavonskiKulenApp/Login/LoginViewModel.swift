//
//  LoginViewModel.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 19.02.2024..
//

import UIKit

class LoginViewModel {
    var pinToken: String
    var userPIN: String
    var digits: [String]
    let keyStorage: UserKeyStorage
    let loginStatusStorage: LoginStatusStorage
    var loginSuccessful: (() -> Void)?
    var resetAndLogout: (() -> Void)?
    
    init(
         pinToken: String,
         userPIN: String,
         digits: [String],
         keyStorage: UserKeyStorage,
         loginStatusStorage: LoginStatusStorage,
         loginSuccessful: (() -> Void)? = nil,
         resetAndLogout: (() -> Void)? = nil
    ) {
        self.pinToken = pinToken
        self.userPIN = userPIN
        self.digits = digits
        self.keyStorage = keyStorage
        self.loginStatusStorage = loginStatusStorage
        self.loginSuccessful = loginSuccessful
        self.resetAndLogout = resetAndLogout
    }
    
    func validateUserPIN(inputPIN: String) -> Bool {
        let savedPIN = keyStorage.loadPIN()
        
        if inputPIN == savedPIN && loginStatusStorage.setLoginStatus(.loggedIn) {
            return true
        }
        else if savedPIN == "PIN_not_found" {
            if keyStorage.savePIN(inputPIN) && loginStatusStorage.setLoginStatus(.loggedIn) {
                return true
            }
            return false
        }
        else {
            return false
        }
    }
    
    func resetPIN(inputPIN: String) -> Bool {
        keyStorage.deletePIN()
        return loginStatusStorage.setLoginStatus(.loggedOut)
    }
}

