//
//  LoginViewController.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 13.02.2024..
//

import UIKit
import Lottie

class LoginViewController: UIViewController {

    var didSelectLogin: () -> Void
    
    init() {
        didSelectLogin = {}
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var animation: LottieAnimationView = {
        var lottie = LottieAnimationView()
        lottie = .init(name: "sausage")
        lottie.loopMode = .loop
        lottie.play()
        lottie.heightAnchor.constraint(lessThanOrEqualToConstant: 250).isActive = true
        lottie.translatesAutoresizingMaskIntoConstraints = false
        return lottie
    }()
    
    private lazy var appTitle: UILabel = {
        let title = UILabel()
        title.text = "SlavonskiKulenApp"
        title.font = UIFont(name: "MadimiOne-Regular", size: 30)
        title.textColor = Design.Colors.Primary.kulenDarkGray
        title.heightAnchor.constraint(equalToConstant: 50).isActive = true
        title.translatesAutoresizingMaskIntoConstraints = false
        return title
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("LOGIN", for: .normal)
        button.titleLabel?.font = UIFont(name: "MadimiOne-Regular", size: 35)
        button.layer.cornerRadius = 6
        button.backgroundColor = Design.Colors.Primary.kulenLightGrey
        button.setTitleColor(Design.Colors.Primary.kulenWhite, for: .normal)
        button.setTitleColor(Design.Colors.Primary.kulenDarkGray, for: .highlighted)
        button.isSelected = false
        button.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        button.heightAnchor.constraint(equalToConstant: 45).isActive = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var stackView: UIStackView = {
       let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Design.Colors.Primary.kulenLightGrey
        addSubviews()
    }
    
    @objc func loginButtonTapped() {
        didSelectLogin()
    }
    
    private func addSubviews() {
        stackView.addArrangedSubview(appTitle)
        stackView.addArrangedSubview(loginButton)
        stackView.addArrangedSubview(animation)
        view.addSubview(stackView)
        constraint()
    }
    
    private func constraint() {
        stackView.topAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        stackView.leadingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        stackView.trailingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        stackView.bottomAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -150).isActive = true
    }
}
