//
//  PINViewController.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 13.02.2024..
//

import UIKit

class PINViewController: UIViewController {
    
    let loginModel: LoginViewModel
    
    init(loginModel: LoginViewModel) {
        self.loginModel = loginModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let keys = [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9],
        [0]
    ]
    
    private lazy var containerView: UIView = {
       let view = UIView()
        view.widthAnchor.constraint(equalToConstant: 300).isActive = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var pinLabel: UILabel = {
       let label = UILabel()
        label.numberOfLines = 1
        label.text = "Type PIN"
        label.font = UIFont(name: "MadimiOne-Regular", size: 26)
        label.widthAnchor.constraint(equalToConstant: 240).isActive = true
        label.textColor = Design.Colors.Primary.kulenDarkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var deleteKeyButton: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(systemName: "delete.left"), for: .normal)
        button.tintColor = Design.Colors.Primary.kulenDarkGray
        button.addTarget(self, action: #selector(deleteKey), for: .touchUpInside)
        button.widthAnchor.constraint(equalToConstant: 60).isActive = true
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var containerStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fillEqually
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var keypadStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .fill
        stack.spacing = 10
        stack.distribution = .fillEqually
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("LOGIN", for: .normal)
        button.titleLabel?.font = UIFont(name: "MadimiOne-Regular", size: 35)
        button.setTitleColor(Design.Colors.Primary.kulenWhite, for: .normal)
        button.setTitleColor(Design.Colors.Primary.kulenDarkGray, for: .highlighted)
        button.widthAnchor.constraint(equalToConstant: 120).isActive = true
        button.heightAnchor.constraint(lessThanOrEqualToConstant: 40).isActive = true
        button.addTarget(self, action: #selector(loginButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var resetPinButton: UIButton = {
        let button = UIButton()
        button.setTitle("Reset PIN", for: .normal)
        button.titleLabel?.font = UIFont(name: "MadimiOne-Regular", size: 24)
        button.setTitleColor(Design.Colors.Primary.kulenDarkRed, for: .normal)
        button.setTitleColor(Design.Colors.Primary.kulenDarkGray, for: .highlighted)
        button.widthAnchor.constraint(equalToConstant: 120).isActive = true
        button.heightAnchor.constraint(lessThanOrEqualToConstant: 40).isActive = true
        button.addTarget(self, action: #selector(resetButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Design.Colors.Primary.kulenLightGrey
        setupKeypad()
        addSubviews()
    }
    
    private func setupKeypad() {
        loginModel.userPIN.removeAll()
        for row in keys {
            let keypadRowStack = UIStackView()
            keypadRowStack.axis = .horizontal
            keypadRowStack.alignment = .center
            keypadRowStack.distribution = .fillEqually
            keypadRowStack.spacing = 10
            keypadRowStack.translatesAutoresizingMaskIntoConstraints = false
            
            for key in row {
                let button = createKeyButton(for: key)
                keypadRowStack.addArrangedSubview(button)
            }
            keypadStack.addArrangedSubview(keypadRowStack)
        }
    }
    
    private func createKeyButton(for key: Int) -> UIButton {
        let button = UIButton(type: .system)
        button.setTitle("\(key)", for: .normal)
        button.setTitleColor(Design.Colors.Primary.kulenDarkGray, for: .normal)
        button.setTitleColor(Design.Colors.Primary.kulenDarkRed, for: .highlighted)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 28)
        button.heightAnchor.constraint(equalToConstant: 100).isActive = true
        button.widthAnchor.constraint(equalToConstant: 100).isActive = true
        button.layer.cornerRadius = 8
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(digitButtonTapped(sender:)), for: .touchUpInside)
        button.backgroundColor = Design.Colors.Primary.kulenDarkGray.withAlphaComponent(0.35)
        return button
    }
    
    @objc func digitButtonTapped(sender: UIButton) {
        guard let digitalString = sender.title(for: .normal) else { return }
        if loginModel.digits.count < 8 {
            loginModel.digits.append(digitalString)
            loginModel.userPIN = loginModel.digits.joined()
            reloadPinLabel()
        }
    }
    
    @objc func deleteKey() {
        if !loginModel.digits.isEmpty {
            loginModel.digits.removeLast()
            reloadPinLabel()
        }
    }
    
    func reloadPinLabel() {
        loginModel.userPIN = loginModel.digits.joined()
        pinLabel.text = loginModel.userPIN
        if loginModel.digits.isEmpty {
            pinLabel.text = "Type PIN"
        }
    }
    
    @objc func loginButtonTapped() {
        if !loginModel.userPIN.isEmpty {
            if loginModel.validateUserPIN(inputPIN: loginModel.userPIN) {
                loginModel.loginSuccessful?()
            }
            else {
                pinLabel.text = "Invalid PIN" 
            }
        }
        else {
            pinLabel.text = "Must enter PIN"
        }
        loginModel.digits.removeAll()
        loginModel.userPIN.removeAll()
    }
    
    @objc func resetButtonTapped() {
        if loginModel.resetPIN(inputPIN: loginModel.userPIN) {
            loginModel.resetAndLogout?()
        }
    }
    
    private func addSubviews() {
        containerView.addSubview(pinLabel)
        containerView.addSubview(deleteKeyButton)
        containerStack.addArrangedSubview(containerView)
        containerStack.addArrangedSubview(keypadStack)
        containerStack.addArrangedSubview(loginButton)
        containerStack.addArrangedSubview(resetPinButton)
        view.addSubview(containerStack)
        constraint()
    }
    
    private func constraint() {
        pinLabel.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        pinLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor).isActive = true
        pinLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        deleteKeyButton.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        deleteKeyButton.trailingAnchor.constraint(equalTo: containerView.trailingAnchor).isActive = true
        deleteKeyButton.bottomAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        containerStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 100).isActive = true
        containerStack.leadingAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        containerStack.trailingAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        containerStack.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
    }
}
