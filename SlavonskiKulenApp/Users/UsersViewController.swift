//
//  SecondViewController.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 10.01.2024..
//

import UIKit
import Combine

class UsersViewController: UIViewController {

    let usersViewModel = UsersViewModel()
    let activityIndicator = SlavonskiKulenActivityIndicator()
    var refreshControl = SlavonskiKulenRefreshControl()
    var popUpAlert: (() -> ())?
    private var subscriptionBag = Set<AnyCancellable>()
    
    private lazy var tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(UsersTableViewCell.self, forCellReuseIdentifier: UsersTableViewCell.identifier)
        table.backgroundColor = Design.Colors.Primary.kulenLightGrey
        table.separatorColor = Design.Colors.Primary.kulenLightGrey
        table.layer.cornerRadius = 10
        table.delegate = self
        table.dataSource = self
        return table
    }()
    
    private lazy var searchBar: UISearchBar = {
       let searchBar = UISearchBar()
        searchBar.searchTextField.attributedPlaceholder = NSAttributedString(string: "Search names", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        searchBar.searchTextField.clearButtonMode = .always
        searchBar.searchTextField.textColor = .white
        searchBar.barTintColor = Design.Colors.Primary.kulenDarkGray
        searchBar.searchTextField.backgroundColor = Design.Colors.Primary.kulenLightGrey
        searchBar.tintColor = .white
        searchBar.searchTextField.leftView?.tintColor = .white
        searchBar.searchTextField.layer.cornerRadius = 10
        searchBar.delegate = self
        searchBar.searchTextField.layer.masksToBounds = true
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        return searchBar
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Design.Colors.Primary.kulenDarkGray
        refreshControl.add(to: self.tableView)
        setupRefreshControl()
        addSubviews()
        bind()
        usersViewModel.fetchUserData()
    }
    
    private func bind() {
        usersViewModel.state = .loading
        
        usersViewModel.$displayedUsers
            .combineLatest(usersViewModel.$state)
            .receive(on: RunLoop.main)
            .sink { [weak self] ( _ , _ ) in
                self?.reloadState()
            }
            .store(in: &subscriptionBag)
        
        usersViewModel.textFieldPublisher
            .receive(on: RunLoop.main)
            .sink { filterInput in
                self.usersViewModel.filterUsers(with: filterInput)
                self.usersViewModel.state = .loaded
            }
            .store(in: &subscriptionBag)
    }
    
    func reloadState() {
        switch usersViewModel.state {
        case .empty:
            ()
        case .loading:
            activityIndicator.show(in: self.tableView)
        case .loaded:
            reloadTableData()
            activityIndicator.hide(from: self.tableView)
        }
    }
    
    private func reloadTableData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func setupRefreshControl() {
        refreshControl.onRefresh = { [weak self] in
            DispatchQueue.main.async {
                self?.refreshControl.start()
                self?.usersViewModel.fetchUserData()
                self?.reloadTableData()
                self?.refreshControl.stop()
            }
        }
    }
    
    private func addSubviews() {
        view.addSubview(searchBar)
        view.addSubview(tableView)
        constraint()
    }
    
    private func constraint() {
        searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        searchBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        searchBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        searchBar.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor, constant: 8).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
    }
}

extension UsersViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: UsersTableViewCell.identifier, for: indexPath) as? UsersTableViewCell else {
            return UITableViewCell()
        }
        let data = usersViewModel.displayedUsers[indexPath.row]
        cell.setupUI(user: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { _, _, _ in
            tableView.beginUpdates()
            self.usersViewModel.users.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
            tableView.endUpdates()
        }
        delete.backgroundColor = Design.Colors.Secondary.kulenRed
        let swipe = UISwipeActionsConfiguration(actions: [delete])
        return swipe
    }
}

extension UsersViewController: UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersViewModel.displayedUsers.count
    }
}

extension UsersViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        usersViewModel.displayedUsers = usersViewModel.users
        if searchBar.searchTextField.text == "" {
            popUpAlert?()
        } else {
            if let filterInput = searchBar.searchTextField.text { usersViewModel.textFieldPublisher.send(filterInput) }
        }
    }
}
