//
//  UserTableViewCell.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 10.01.2024..
//

import UIKit

class UsersTableViewCell: UITableViewCell {

    static let identifier: String = "userCell"
    
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .leading
        stack.distribution = .equalCentering
        stack.layer.cornerRadius = 10
        stack.spacing = 10
        stack.backgroundColor = Design.Colors.Primary.kulenDarkGray
        stack.layoutMargins = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        stack.isLayoutMarginsRelativeArrangement = true
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private lazy var userIdLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var usernameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var userEmailLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var streetLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var suiteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var cityLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var zipcodeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var latitudeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var longitudeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        return label
    }()
    
    private lazy var userPhoneLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var websiteLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var companyNameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var companyCatchPhraseLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 0
        return label
    }()
    
    private lazy var companyBsLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 0
        return label
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        contentView.backgroundColor = Design.Colors.Primary.kulenLightGrey
        addSubviews()
    }
    
    private func addSubviews() {
        stackView.addArrangedSubview(userIdLabel)
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(usernameLabel)
        stackView.addArrangedSubview(userEmailLabel)
        stackView.addArrangedSubview(streetLabel)
        stackView.addArrangedSubview(suiteLabel)
        stackView.addArrangedSubview(cityLabel)
        stackView.addArrangedSubview(zipcodeLabel)
        stackView.addArrangedSubview(latitudeLabel)
        stackView.addArrangedSubview(longitudeLabel)
        stackView.addArrangedSubview(userPhoneLabel)
        stackView.addArrangedSubview(websiteLabel)
        stackView.addArrangedSubview(companyNameLabel)
        stackView.addArrangedSubview(companyCatchPhraseLabel)
        stackView.addArrangedSubview(companyBsLabel)
        contentView.addSubview(stackView)
        constraint()
    }
    
    private func constraint() {
        stackView.topAnchor.constraint(greaterThanOrEqualTo: contentView.topAnchor, constant: 5).isActive = true
        stackView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8).isActive = true
        stackView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8).isActive = true
        stackView.bottomAnchor.constraint(lessThanOrEqualTo: contentView.bottomAnchor, constant: -5).isActive = true
    }
    
    func setupUI(user: User) {
        userIdLabel.text = "ID: \(String(user.id ?? -1))"
        nameLabel.text = "Name: \(user.name ?? "Error")"
        usernameLabel.text = "Username: \(user.username ?? "Error")"
        userEmailLabel.text = "E-mail: \(user.email ?? "Error")"
        streetLabel.text = "Street: \(user.address?.street ?? "Error")"
        suiteLabel.text = "Suite: \(user.address?.suite ?? "Error")"
        cityLabel.text = "City: \(user.address?.city ?? "Error")"
        zipcodeLabel.text = "ZIP: \(user.address?.zipcode ?? "Error")"
        latitudeLabel.text = "Latitude: \(user.address?.geo?.lat ?? "Error")"
        longitudeLabel.text = "Longitude: \(user.address?.geo?.lng ?? "Error")"
        userPhoneLabel.text = "Phone: \(user.phone ?? "Error")"
        websiteLabel.text = "Website: \(user.website ?? "Error")"
        companyNameLabel.text = "Company name: \(user.company?.name ?? "Error")"
        companyCatchPhraseLabel.text = "Company phrase: \(user.company?.catchPhrase ?? "Error")"
        companyBsLabel.text = "Company BS: \(user.company?.bs ?? "Error")"
    }
}
