//
//  UsersViewModel.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 10.01.2024..
//

import Foundation
import Combine

enum State {
    case loading
    case loaded
    case empty
}

class UsersViewModel: ObservableObject {
    
    @Published var displayedUsers: [User] = []
    var textFieldPublisher = PassthroughSubject<String, Never>()
    var users: [User] = []
    private var bag = Set<AnyCancellable>()
    private var filteredUsersBag = Set<AnyCancellable>()
    @Published var state: State = .empty
    
    func fetchUserData()  {
        ApiCaller.shared.fetchUsers()
            .sink(receiveCompletion: { _ in
            }, receiveValue: { result in
                self.displayedUsers = result
                self.users = result
                self.state = .loaded
            })
            .store(in: &bag)
    }
    
    func filterUsers(with filterInput: String) {
        if filterInput.isEmpty || filterInput.starts(with: " ") {
            displayedUsers = users
            return
        }
        displayedUsers = displayedUsers.filter({ user in
            let username = user.name?.lowercased()
            return username?.starts(with: filterInput.lowercased()) ?? false
        })
    }
}
