//
//  ApiCaller.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 14.12.2023..
//

import Foundation
import Combine

class ApiCaller {
    
    static let shared = ApiCaller()
    
    init(){}
    
    enum FetchPostsError: Error {
        case custom(error: Error)
        case wrongUrl
        case unknown
        case responseNotOk
        
        var errorDescription: String? {
            switch self {
            case .custom:
                return "Sth went wrong."
            case .unknown:
                return "Unknown decode error."
            case .wrongUrl:
                return "Wrong URL"
            case .responseNotOk:
                return "HTTP Response not 2xx."
            }
        }
    }
    
    func fetchPosts() -> AnyPublisher<[Post], Error> {
        
        let postsURL = Urls.postsURL
        if let url = URL(string: postsURL) {
            return URLSession
                .shared
                .dataTaskPublisher(for: url)
                .receive(on: DispatchQueue.main)
                .tryMap({ res in
                    guard let response = res.response as? HTTPURLResponse,
                          response.statusCode >= 200 && response.statusCode <= 300
                    else {
                        throw FetchPostsError.responseNotOk
                    }
                    
                    let decoder = JSONDecoder()
                    guard let posts = try? decoder.decode([Post].self, from: res.data) else {
                        throw FetchPostsError.unknown
                    }
                    return posts
                })
                .eraseToAnyPublisher()
        }
        return Fail(outputType: [Post].self, failure: FetchPostsError.unknown).eraseToAnyPublisher()
    }
    
    func fetchUsers() -> AnyPublisher<[User], Error> {
        
        let userURL = Urls.usersURL
        if let url = URL(string: userURL) {
            return URLSession
                .shared
                .dataTaskPublisher(for: url)
                .receive(on: DispatchQueue.main)
                .tryMap({ res in
                    guard let response = res.response as? HTTPURLResponse,
                          response.statusCode >= 200 && response.statusCode <= 300
                    else {
                        throw FetchPostsError.responseNotOk
                    }
                    let decoder = JSONDecoder()
                    guard let users = try? decoder.decode([User].self, from: res.data) else {
                        throw FetchPostsError.unknown
                    }
                    return users
                })
                .eraseToAnyPublisher()
        }
        return Fail(outputType: [User].self, failure: FetchPostsError.unknown).eraseToAnyPublisher()
    }
}
