//
//  UserDefaultsKeyStorage.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 28.02.2024..
//

import UIKit

class UserDefaultsKeyStorage: UserKeyStorage {
    let base: UserDefaults
    let pinKey: String
    
    init(base: UserDefaults, pinKey: String) {
        self.base = base
        self.pinKey = pinKey
    }
    
    func savePIN(_ pin: String) -> Bool {
        base.set(pin, forKey: pinKey)
        return true
    }
    
    func loadPIN() -> String {
        let savedPIN = base.string(forKey: pinKey) ?? ""
        return savedPIN
    }
    
    func deletePIN() {
        let savedPIN = base.string(forKey: pinKey)
        if savedPIN != nil {
            base.removeObject(forKey: pinKey)
        }
    }
}
