//
//  KeychainLoginStatusStorage.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 29.02.2024..
//

import UIKit
import Security

protocol LoginStatusStorage {
    func setLoginStatus(_ loginStatus: LoginStatus) -> Bool
    func loadLoginStatus() -> String
}

class KeychainLoginStatusStorage: LoginStatusStorage {
    
    let service: String
    let account: String
    
    init(service: String, account: String) {
        self.service = service
        self.account = account
    }
    
    func setLoginStatus(_ loginStatus: LoginStatus) -> Bool {
        let state = Data(loginStatus.rawValue.utf8)
        
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: self.service,
            kSecAttrAccount as String: self.account,
            kSecValueData as String: state
        ]
        SecItemDelete(query as CFDictionary)
        let status = SecItemAdd(query as CFDictionary, nil)
        if status == errSecSuccess {
            return true
        }
        return false
    }
    
    func loadLoginStatus() -> String {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: self.service,
            kSecAttrAccount as String: self.account,
            kSecReturnData as String: kCFBooleanTrue!,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]
        var item: CFTypeRef?
        let status = SecItemCopyMatching(query as CFDictionary, &item)
        if status == errSecSuccess, let data = item as? Data, let state = String(data: data, encoding: .utf8) {
            return state
        }
        
        return "loggedOut"
    }
    
    func deleteLoginState() {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: self.service,
            kSecAttrAccount as String: self.account
        ]
        SecItemDelete(query as CFDictionary)
    }
}
