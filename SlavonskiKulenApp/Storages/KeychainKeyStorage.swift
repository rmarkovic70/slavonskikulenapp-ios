//
//  KeychainKeyStorage.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 01.03.2024..
//

import UIKit
import Security

protocol UserKeyStorage {
    func savePIN(_ pin: String) -> Bool
    func loadPIN() -> String
    func deletePIN()
}

class KeychainKeyStorage: UserKeyStorage {
    let service: String
    let account: String
    
    init(service: String, account: String) {
        self.service = service
        self.account = account
    }
    
    func savePIN(_ pin: String) -> Bool {
        let pinData = Data(pin.utf8)
        
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: self.service,
            kSecAttrAccount as String: self.account,
            kSecValueData as String: pinData
        ]
        let status = SecItemAdd(query as CFDictionary, nil)
        if status == errSecSuccess {
            return true
        }
        return false
    }
    
    func loadPIN() -> String {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: self.service,
            kSecAttrAccount as String: self.account,
            kSecReturnData as String: kCFBooleanTrue!,
            kSecMatchLimit as String: kSecMatchLimitOne
        ]
        SecItemDelete(query as CFDictionary)
        var item: CFTypeRef?
        let status = SecItemCopyMatching(query as CFDictionary, &item)
        if status == errSecSuccess, let data = item as? Data, let pin = String(data: data, encoding: .utf8) {
            return pin
        }
        return "PIN_not_found"
    }
    
    func deletePIN() {
        let query: [String: Any] = [
            kSecClass as String: kSecClassGenericPassword,
            kSecAttrService as String: self.service,
            kSecAttrAccount as String: self.account
        ]
        SecItemDelete(query as CFDictionary)
    }
}
