//
//  UserDefaultsLoginStatusStorage.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 28.02.2024..
//

import UIKit

class UserDefaultsLoginStatusStorage: LoginStatusStorage {
    
    let base: UserDefaults
    let loginKey: String
    
    init(base: UserDefaults, loginKey: String) {
        self.base = base
        self.loginKey = loginKey
    }
    
    func setLoginStatus(_ loginStatus: LoginStatus) -> Bool {
        base.set(loginStatus.rawValue, forKey: loginKey)
        return true
    }
    
    func loadLoginStatus() -> String {
        guard let rawValue = base.string(forKey: loginKey), let status = LoginStatus(rawValue: rawValue) else {
            return LoginStatus(rawValue: "LoggedOut")?.rawValue ?? ""
        }
        return status.rawValue
    }
}
