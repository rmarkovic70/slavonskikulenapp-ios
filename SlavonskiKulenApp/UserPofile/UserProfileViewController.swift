//
//  UserProfileViewController.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 03.03.2024..
//

import UIKit

class UserProfileViewController: UIViewController {

    let userProfileModel: UserProfileViewModel
    let keychainKeyStorage: UserKeyStorage
    let keychainLoginStatusStorage: LoginStatusStorage
    var toMainScreen: (()->())
    
    init(keychainKeyStorage: UserKeyStorage, keychainLoginStatusStorage: LoginStatusStorage, toMainScreen: @escaping (()->())) {
        self.keychainKeyStorage = keychainKeyStorage
        self.keychainLoginStatusStorage = keychainLoginStatusStorage
        self.userProfileModel = UserProfileViewModel(keychainKeyStorage: keychainKeyStorage, keychainLoginStatusStorage: keychainLoginStatusStorage, logout: {})
        self.toMainScreen = toMainScreen
        super.init(nibName: nil, bundle: nil)
        userProfileModel.logout = { [weak self] in
            self?.toMainScreen()
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy var appTitle: UILabel = {
        let label = UILabel()
        label.text = "User profile"
        label.textColor = Design.Colors.Primary.kulenDarkGray
        label.font = UIFont(name: "MadimiOne-Regular", size: 40)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    lazy var setPinTextfield: UITextField = {
        let textfield = UITextField()
        textfield.font = UIFont.boldSystemFont(ofSize: 40)
        textfield.textColor = Design.Colors.Primary.kulenDarkGray
        textfield.text = userProfileModel.keychainKeyStorage.loadPIN()
        textfield.keyboardType = .numberPad
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    lazy var changePinButton: UIButton = {
        let button = UIButton()
        button.setTitle("Change PIN", for: .normal)
        button.setTitleColor(Design.Colors.Primary.kulenLightGrey, for: .normal)
        button.setTitleColor(Design.Colors.Primary.kulenWhite, for: .highlighted)
        button.addTarget(self, action: #selector(setNewPin), for: .touchUpInside)
        button.widthAnchor.constraint(equalToConstant: 260).isActive = true
        button.heightAnchor.constraint(equalToConstant: 80).isActive = true
        button.backgroundColor = Design.Colors.Primary.kulenDarkGray
        button.layer.cornerRadius = 8
        button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var logoutButton: UIButton = {
       let button = UIButton()
        button.setTitle("LOGOUT", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        button.setTitleColor(Design.Colors.Primary.kulenWhite, for: .normal)
        button.setTitleColor(Design.Colors.Primary.kulenDarkGray, for: .highlighted)
        button.widthAnchor.constraint(equalToConstant: 260).isActive = true
        button.heightAnchor.constraint(equalToConstant: 80).isActive = true
        button.backgroundColor = Design.Colors.Primary.kulenDarkRed
        button.layer.cornerRadius = 8
        button.contentEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        button.addTarget(self, action: #selector(logoutButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.alignment = .center
        stack.distribution = .fill
        stack.spacing = 5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Design.Colors.Primary.kulenLightGrey
        addSubviews()
    }

    @objc func logoutButtonTapped() {
        userProfileModel.logoutUser()
    }
    
    @objc func setNewPin() {
        if changePinButton.titleLabel?.text == "Change PIN" {
            setPinTextfield.becomeFirstResponder()
            changePinButton.setTitle("SAVE", for: .normal)
        }
        else if changePinButton.titleLabel?.text == "SAVE" {
            if let newPIN = setPinTextfield.text, !newPIN.isEmpty {
                userProfileModel.keychainKeyStorage.deletePIN()
                if userProfileModel.keychainKeyStorage.savePIN(newPIN) {
                    setPinTextfield.resignFirstResponder()
                    changePinButton.setTitle("Change PIN", for: .normal)
                    setPinTextfield.text = newPIN
                }
            }
        }
    }
    
    private func addSubviews() {
        view.addSubview(appTitle)
        
        stackView.addArrangedSubview(setPinTextfield)
        stackView.addArrangedSubview(changePinButton)
        stackView.addArrangedSubview(logoutButton)
        view.addSubview(stackView)
        constraint()
    }
    
    private func constraint() {
        appTitle.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 50).isActive = true
        appTitle.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        stackView.topAnchor.constraint(greaterThanOrEqualTo: appTitle.bottomAnchor, constant: 50).isActive = true
        stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true
        stackView.bottomAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -300).isActive = true
    }
}
