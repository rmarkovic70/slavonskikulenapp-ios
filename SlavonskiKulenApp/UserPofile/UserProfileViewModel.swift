//
//  UserProfileViewModel.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 03.03.2024..
//

import UIKit

class UserProfileViewModel {
    
    var logout: () -> ()
    let keychainKeyStorage: UserKeyStorage
    let keychainLoginStatusStorage: LoginStatusStorage
    
    init(keychainKeyStorage: UserKeyStorage, keychainLoginStatusStorage: LoginStatusStorage, logout: @escaping ()->()) {
        self.logout = logout
        self.keychainKeyStorage = keychainKeyStorage
        self.keychainLoginStatusStorage = keychainLoginStatusStorage
    }
    
    func logoutUser() {
        if keychainLoginStatusStorage.setLoginStatus(.loggedOut) {
            logout()
        }
        
    }
}
