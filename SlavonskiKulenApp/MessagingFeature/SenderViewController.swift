//
//  TextfieldViewController.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 12.01.2024..
//

import UIKit

class SenderViewController: UIViewController {
    
    var didTapButton: (() -> ())?
    var textfieldViewModel: SenderViewModel?
    
    let inputTextField: UITextField = {
        let textfield = UITextField()
        textfield.placeholder = "Write a message."
        textfield.textAlignment = .center
        textfield.text = ""
        textfield.borderStyle = .roundedRect
        textfield.translatesAutoresizingMaskIntoConstraints = false
        return textfield
    }()
    
    let saveButton: UIButton = {
        let button = UIButton()
        button.tintColor = .white
        button.backgroundColor = Design.Colors.Primary.kulenDarkGray
        button.setTitle("Save", for: .normal)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(saveButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Design.Colors.Primary.kulenLightGrey
        addSubviews()
    }
    
    @objc func saveButtonTapped(_ sender:UIButton) {
        guard let textfieldViewModel = textfieldViewModel else { return }
        textfieldViewModel.message = inputTextField.text ?? "No messsage entered."
        textfieldViewModel.sendDataToParent()
        didTapButton?()
        inputTextField.text = ""
    }
    
    private func addSubviews() {
        view.addSubview(inputTextField)
        view.addSubview(saveButton)
        constraint()
    }
    
    private func constraint() {
        saveButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        saveButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        saveButton.widthAnchor.constraint(equalToConstant: 250).isActive = true
        saveButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        inputTextField.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        inputTextField.bottomAnchor.constraint(equalTo: saveButton.topAnchor, constant: -5).isActive = true
        inputTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        inputTextField.heightAnchor.constraint(equalToConstant: 30).isActive = true
    }
}
