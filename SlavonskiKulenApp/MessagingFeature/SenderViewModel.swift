//
//  TextfieldViewModel.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 12.01.2024..
//

import UIKit
import Combine

class SenderViewModel: ObservableObject {
    
    let myUITextFieldPublisher = PassthroughSubject<String, Never>()
    var message = ""
    
    func sendDataToParent() {
        myUITextFieldPublisher.send(message)
    }
}
