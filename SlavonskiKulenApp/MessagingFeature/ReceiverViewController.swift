//
//  ButtonViewController.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 11.01.2024..
//

import UIKit
import Combine

class ReceiverViewController: UIViewController {
    
    var textfieldViewModel = SenderViewModel()
    var didTapButton: ((SenderViewModel) -> ())?
    private var bag = Set<AnyCancellable>()
    
    private lazy var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.textColor = Design.Text.Colors.kulenWhite
        label.numberOfLines = 1
        label.textAlignment = .center
        return label
    }()
    
    private lazy var pressMeButton: UIButton = {
        let button = UIButton()
        button.tintColor = .white
        button.backgroundColor = Design.Colors.Primary.kulenDarkGray
        button.setTitle("Press me", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.setTitleColor(.black, for: .selected)
        button.layer.cornerRadius = 5
        button.addTarget(self, action: #selector(pressMeButtonTapped), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Design.Colors.Primary.kulenLightGrey
        addSubviews()
        setupLabel()
        setupSubscriber()
    }
    
    @objc func pressMeButtonTapped(_ sender:UIButton) {
        didTapButton?(textfieldViewModel)
    }
    
    private func addSubviews() {
        view.addSubview(messageLabel)
        view.addSubview(pressMeButton)
        constraint()
    }
    
    func setupSubscriber() {
        textfieldViewModel.myUITextFieldPublisher
            .receive(on: RunLoop.main)
            .sink { [weak self] newText in
                self?.messageLabel.text = newText
            }
            .store(in: &bag)
    }
    
    private func setupLabel() {
        messageLabel.text = "This is a place where a message is shown."
    }
    
    private func constraint() {
        pressMeButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pressMeButton.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        pressMeButton.widthAnchor.constraint(equalToConstant: 250).isActive = true
        pressMeButton.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        messageLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        messageLabel.bottomAnchor.constraint(equalTo: pressMeButton.topAnchor, constant: -10).isActive = true
    }
}
