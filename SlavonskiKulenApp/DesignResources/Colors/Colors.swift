//
//  AppConstants.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 07.01.2024..
//

import Foundation
import UIKit

struct Design {
    
    struct Text {
        
        struct Colors {
            static let kulenWhite = UIColor.white
        }
        
        struct Fonts {
            
        }
    }
    
    struct Colors {
        
        struct Primary {
            static let kulenLightGrey = UIColor(red: 90.0/255.0, green: 96.0/255.0, blue: 103.0/255.0, alpha: 1)
            static let kulenDarkGray = UIColor(red: 39.0/255.0, green: 40.0/255.0, blue: 43.0/255.0, alpha: 1)
            static let kulenDarkRed = UIColor(red: 140.0/255.0, green: 25.0/255.0, blue: 0/0, alpha: 1)
            static let kulenWhite = UIColor.white
        }
        
        struct Secondary {
            static let kulenRed = UIColor(red: 242.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 22)
            static let kulenLightGreen = UIColor(red: 117.0/255.0, green: 123.0/255.0, blue: 112.0/255.0, alpha: 1)
        }
         
    }
}

struct Urls {
    static let postsURL = "https://jsonplaceholder.typicode.com/posts"
    static let usersURL = "https://jsonplaceholder.typicode.com/users"
}
