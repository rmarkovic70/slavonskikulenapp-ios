//
//  ClasicAlert.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 09.02.2024..
//

import UIKit

class Alerts {
    
    static let manager = Alerts()
    
    func createBasicAlert(title: String, message: String) -> UIAlertController {
        let alertMessagePopUpBox = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Got it", style: .default)
        alertMessagePopUpBox.addAction(okButton)
        return alertMessagePopUpBox
    }
}
