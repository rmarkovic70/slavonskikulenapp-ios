//
//  User.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 14.12.2023..
//

import Foundation

struct Post: Codable {
    let userId, id: Int?
    let title, body: String?
}
