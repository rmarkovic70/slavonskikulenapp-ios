//
//  User.swift
//  SlavonskiKulenApp
//
//  Created by Apis IT on 10.01.2024..
//

import Foundation

struct User: Codable {
    let id: Int?
    let name, username, email, phone, website: String?
    let address: Address?
    let company: Company?
}

struct Address: Codable {
    let street, suite, city, zipcode: String?
    let geo: Geo?
}

struct Geo: Codable {
    let lat, lng: String?
}

struct Company: Codable {
    let name, catchPhrase, bs: String?
}
